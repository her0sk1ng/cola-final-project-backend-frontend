# FrontEnd

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Project Deployment

Project is deployed on AWS Stack - RDS with MySQL, EC2 for the backend, S3 for frontend and CloudFront instance. It's available either via https://d7y0othjd9xzp.cloudfront.net or https://prizecheck.info

## Credentials for testing

Application is split into administrator and users parts. Following credentials can be used to test both:

admin@cola.com / P@ssw0rd!  
ivanov@metro.com / P@ssw0rd!

## Workflow

Public Home Page:

![Home Page](images/home.png)

User login:

![User Login](images/te.png)

Admin login:

![Admin login](images/admin_login.png)

Scanned component:

![Code Scan](images/code_scan.png)

Winning code:

![Winning Code](images/winning_code.png)

Redeem code dialog:

![Redeem Dialog](images/redeem_dialog.png)

```bash
  Dialog Yes--> Scan prize barcode;
  Dialog No--> Redeem without prize barcode;
```

Invalid barcode:

![Invalid Barcode](images/invalid_barcode.png)

Already Redeemed:

![Already Redeemed](images/already_redeemed.png)

Report Redemption Attempt - sends email to hardcoded admin address:

![Report Attempt](images/repor_redemption_attempt.png)

Check Redemption Details:

![Check Details](images/check_details.png)

Contact helpdesl - sends email to hardcoded admin address:

![Send Email](images/helpdesk.png)

User menu options:

![User Menu](images/user_menu.png)

User activities:

![User Activities](images/user_activities.png)

```bash
  Status successful --> code was redeemed by this user;
  Status redeemed --> code was already redeemed previously;
  Status cancelled --> winning code was scanned, but not redeemed afterwards;
  Status invalid --> scanned code is not winning;
```

User's Outlet activities:

![Outlet Activities](images/outlet_activities.png)

Theme's selector:

![Themes](images/themes.png)

Admin view:

![Admin](images/admin_view.png)

Create user (similar for brand and customer):

![Create User](images/create_user.png)

Edit user (similar for brand and customer):

![Edit User](images/edit_user.png)

User activities (login/logoug):

![User Activities](images/admin_user_activities.png)

Redemption Activities:

![Admin Reports](images/admin_reports.png)

Dashboard:

![Dashboard](images/dashboard.png)
