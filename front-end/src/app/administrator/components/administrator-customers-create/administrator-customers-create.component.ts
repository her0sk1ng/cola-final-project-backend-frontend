import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CreateCustomer } from 'src/app/common/models/create-customer';
import { Customer } from 'src/app/common/models/customer';
import { CustomerService } from 'src/app/core/services/customer.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-administrator-customers-create',
  templateUrl: './administrator-customers-create.component.html',
  styleUrls: ['./administrator-customers-create.component.scss'],
})
export class AdministratorCustomersCreateComponent implements OnInit {
  public createCustomerForm: FormGroup;
  public customer: Customer;
  public isComponentInCreateState = true;

  constructor(
    private readonly customerService: CustomerService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
    private readonly routeForTheId: ActivatedRoute
  ) {}

  public ngOnInit() {
    this.createFormForTheCustomer();
    this.createOrEditStateCheck();
  }

  public createOrEditStateCheck() {
    const id: string = this.routeForTheId.snapshot.paramMap.get('id');
    if (id === 'customerscreate') {
      this.isComponentInCreateState = true;
    } else {
      this.isComponentInCreateState = false;
      this.putCustomerDetailsIntoEditForm();
    }
  }

  public createFormForTheCustomer() {
    this.createCustomerForm = this.formBuilder.group({
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(30),
        ],
      ],
    });
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.createCustomerForm.controls[controlName].hasError(errorName);
  };

  public createCustomer(): void {
    const createTheCustomer: CreateCustomer = this.createCustomerForm.value;

    this.customerService.createCustomer(createTheCustomer).subscribe(
      () => {
        this.notificator.success(`Succesfully created the Customer!`);
        this.router.navigate([`administrator/customers`]);
      },
      () => {
        this.notificator.error(`Customer creation failed!`);
      }
    );
  }

  public editCustomer(): void {
    const editTheCustomer: CreateCustomer = this.createCustomerForm.value;
    const id: string = this.routeForTheId.snapshot.paramMap.get('id');
    this.customerService.updateCustomer(id, editTheCustomer).subscribe(
      () => {
        this.notificator.success(`Succesfully updated the Customer!`);
        this.router.navigate([`administrator/customers`]);
      },
      () => {
        this.notificator.error(`Customer Update Failed`);
      }
    );
  }

  public putCustomerDetailsIntoEditForm() {
    const id: string = this.routeForTheId.snapshot.paramMap.get('id');
    this.customerService.getCustomer(id).subscribe((data: Customer) => {
      this.customer = data;
      this.createCustomerForm.patchValue({
        name: this.customer.name,
      });
    });
  }
}
