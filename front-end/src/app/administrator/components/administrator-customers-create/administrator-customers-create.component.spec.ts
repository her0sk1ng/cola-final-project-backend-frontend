import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministratorCustomersCreateComponent } from './administrator-customers-create.component';

describe('AdministratorCustomersCreateComponent', () => {
  let component: AdministratorCustomersCreateComponent;
  let fixture: ComponentFixture<AdministratorCustomersCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdministratorCustomersCreateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministratorCustomersCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
