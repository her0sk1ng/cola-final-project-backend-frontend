import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardBarchartCustomersComponent } from './dashboard-barchart-customers.component';

describe('DashboardBarchartCustomersComponent', () => {
  let component: DashboardBarchartCustomersComponent;
  let fixture: ComponentFixture<DashboardBarchartCustomersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardBarchartCustomersComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardBarchartCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
