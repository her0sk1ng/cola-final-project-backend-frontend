import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Subscription } from 'rxjs';
import { UserActivities } from 'src/app/common/models/user-activities';
import { AuthService } from 'src/app/core/services/auth.service';
import { UsersService } from 'src/app/core/services/users.service';
import { FormGroup, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { RedemptionActivity } from 'src/app/common/models/redemption-activity';
import * as moment from 'moment';
import { DataExportService } from 'src/app/core/services/data-export.service';

@Component({
  selector: 'app-administrator-user-activities',
  templateUrl: './administrator-user-activities.component.html',
  styleUrls: ['./administrator-user-activities.component.scss'],
})
export class AdministratorUserActivitiesComponent implements OnInit, OnDestroy {
  // this is build in pagination and sorting for MatTableDataSource
  @ViewChild(MatPaginator) public paginator: MatPaginator;
  @ViewChild(MatSort) public sort: MatSort;

  // this is the data used in the table
  public dataSource: MatTableDataSource<any>;

  public columnsToDisplay = [
    'id',
    'description',
    'date',
    'customer',
    'outlet',
    'visibleUser',
  ];

  public userActivities: UserActivities[];
  public username = '';
  public isAdmin = false;
  public filterUser = '';
  public filterForm = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
  });
  public pipe: DatePipe;
  public searchString: string;
  public fromDateFilter = '';
  public toDateFilter = '';
  public filterString = '';
  public dateFilterFlag = false;

  private subscription: Subscription;
  private roleSubscription: Subscription;

  constructor(
    private readonly userService: UsersService,
    private readonly auth: AuthService,
    private readonly exportData: DataExportService,
  ) {}

  public ngOnInit() {
    this.checkUserRole();
    this.checkUserSub();
    this.getAllUserActivities();
  }

  public checkUserSub() {
    this.subscription = this.auth.user$.subscribe(username => {
      if (username === null) {
        this.username = '';
        // this.notificator.success(`You have logged out.`);
      } else {
        this.username = username;
      }
    });
  }

  public checkUserRole() {
    this.roleSubscription = this.auth.role$.subscribe(role => {
      if (role === 'admin') {
        this.isAdmin = true;
      } else {
        this.isAdmin = false;
      }
    });
  }

  public getAllUserActivities(): void {
    this.userService
      .getAllUserActivities()
      .subscribe((data: UserActivities[]) => {
        this.userActivities = data;
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.getDateRange();
      });
  }

  get fromDate() {
    return this.filterForm.get('fromDate').value;
  }

  get toDate() {
    return this.filterForm.get('toDate').value;
  }

  public applyFilter(filterValue: string) {
    this.filterString = filterValue.trim().toLowerCase();
    this.dataSource.filter = filterValue;
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public getDateRange() {
    this.pipe = new DatePipe('en');
    this.dataSource.filterPredicate = (data: UserActivities, filter) => {
      let result = true;
      const description =
        data.description
          .toLowerCase()
          .indexOf(this.filterString.toLowerCase()) >= 0;
      const brand =
        data.customer.toLowerCase().indexOf(this.filterString.toLowerCase()) >=
        0;
      const outlet =
        data.outlet.toLowerCase().indexOf(this.filterString.toLowerCase()) >= 0;
      const user =
        data.visibleUser
          .toLowerCase()
          .indexOf(this.filterString.toLowerCase()) >= 0;

      if (this.fromDate && this.toDate) {
        this.dateFilterFlag = true;
        const actualDate = Date.parse((data as any).date);
        const fromDate = Date.parse(
          moment(this.fromDate, 'DD/MM/YYYY', true).format(),
        );

        const toDate = Date.parse(
          moment(this.toDate, 'DD/MM/YYYY', true).format(),
        );

        result = actualDate >= fromDate && actualDate <= toDate;
      }

      if (!result) {
        return false;
      }

      if (this.filterString) {
        result =
          (result && description) ||
          (result && brand) ||
          (result && outlet) ||
          (result && user);
      }

      return result;
    };
  }
  public ngOnDestroy(): void {
    this.roleSubscription.unsubscribe();
    this.subscription.unsubscribe();
  }

  public applyDateFilter() {
    this.dataSource.filter = '' + Math.random();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public clearDateFilter() {
    this.dataSource.filter = '';
    this.filterString = '';
    this.toDateFilter = '';
    this.fromDateFilter = '';
    this.dateFilterFlag = false;
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public exportCsv(): void {
    this.exportData.exportCsv(
      this.dataSource.filteredData,
      'Prize Redemption Report',
    );
  }

  exportAsXLSX(): void {
    this.exportData.exportAsExcelFile(
      this.dataSource.filteredData,
      'Prize Redemption Report',
    );
  }
}
