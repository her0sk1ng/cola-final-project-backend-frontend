import { async, TestBed, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
import { AdministratorOutletsCreateComponent } from './administrator-outlets-create.component';
import { of } from 'rxjs';
import { AdministratorModule } from '../../administrator.module';
import { OutletService } from 'src/app/core/services/outlet.service';
import { CustomerService } from 'src/app/core/services/customer.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { FormBuilder, FormsModule, FormGroup, FormControl, FormControlName, Validators } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { CoreModule } from 'src/app/core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { detectChanges } from '@angular/core/src/render3';
import {
  NO_ERRORS_SCHEMA,
  Directive,
  Input,
  HostListener,
  DebugElement,
} from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Customer } from 'src/app/common/models/customer';
import { Outlet } from 'src/app/common/models/outlet';
import { By } from '@angular/platform-browser';
import { by } from 'protractor';

fdescribe('AdministratorOutletsCreateComponent', () => {
  let fixture: ComponentFixture<AdministratorOutletsCreateComponent>;
  let component: AdministratorOutletsCreateComponent;
  const formBuilder: FormBuilder = new FormBuilder();

  const mockBrands: Customer[] = [
    {
      id: '1',
      name: 'firstBrand',
    },
    {
      id: '2',
      name: 'secondBrand',
    },
  ];

  const mockOutlet: Outlet = {
    id: '1',
    name: 'firstOutlet',
    newBrand: 'kaufland',
  };

  const outletService = jasmine.createSpyObj('OutletService', [
    'getAllOutlets',
    'getOutlet',
    'createOutlet',
    'deleteOutlet',
    'updateOutlet',
  ]);

  const customerService = jasmine.createSpyObj('CustomerService', [
    'getAllCustomers',
    'getCustomer',
    'createCustomer',
    'deleteCustomer',
    'updateCustomer',
  ]);

  const router = jasmine.createSpyObj('Router', ['navigate']);
  const notificator = jasmine.createSpyObj('NotificatorService', [
    'success',
    'error',
  ]);
  const activatedRoute = jasmine.createSpyObj('ActivatedRoute', ['params']);

  const mockActivatedRoute = {
    params: of({ id: 1 }),
    snapshot: {
      paramMap: {
        get: () => {
          return 'outletscreate';
        },
      },
    },
  };

  const wrongActivatedRoute = {
    params: of({ id: 1 }),
    snapshot: {
      paramMap: {
        get: () => {
          return '123123123';
        },
      },
    },
  };



  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        AdministratorModule,
        SharedModule,
        CoreModule,
        RouterTestingModule,
        FormsModule,
        NoopAnimationsModule,
      ],
      providers: [
        {
          provide: OutletService,
          useValue: outletService,
        },
        {
          provide: CustomerService,
          useValue: customerService,
        },
        {
          provide: Router,
          useValue: router,
        },
        {
          provide: NotificatorService,
          useValue: notificator,
        },
        {
          provide: ActivatedRoute,
          useValue: mockActivatedRoute,
        },
        {
          provide: FormBuilder,
          useValue: formBuilder
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministratorOutletsCreateComponent);
    component = fixture.componentInstance;
    customerService.getAllCustomers.and.returnValue(of(mockBrands));
    outletService.getOutlet.and.returnValue(of(mockOutlet));
    component.createOutletForm = formBuilder.group({
      name: ['', Validators.required],
      brands: [
        '',
        Validators.required
      ]
    });
    fixture.detectChanges();
  });

  afterEach(() => {
    if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
      (fixture.nativeElement as HTMLElement).remove();
    }
  });

  it('should create the ADministratorOutletsComponent succesfully', () => {
    expect(component).toBeTruthy();
  });

  it('when ComponentInCreateState is true, should have one button', () => {
    component.isComponentInCreateState = true;
    const buttons = fixture.debugElement.queryAllNodes(By.css('button'));
    expect(buttons.length).toEqual(1);
  });

  it('Outlet Name form should be invalid,untoched and pristine when it was not clicked on', () => {

    const input = fixture.nativeElement.querySelector('#outlet-name');
    fixture.detectChanges();
    expect(input.className).toContain('ng-invalid');
    expect(input.className).toContain('ng-untouched');
    expect(input.className).toContain('ng-pristine');
  });

  it('when outlet is created the notificator is called once', () => {
    component.isComponentInCreateState = true;
    fixture.detectChanges();

    const buttonClick = fixture.debugElement.query(By.css('#create'));
    buttonClick.triggerEventHandler('click', null);

    fixture.detectChanges()

    fixture.whenStable().then(() => {
      expect(notificator.succes).toHaveBeenCalledTimes(1);
    });

  });

  it('putting name into outlet name should reflect the outlet and to be touched, valid and dirty', () => {

    fixture.whenStable().then(() => {
      const input = fixture.debugElement.query(By.css('input'));
      const el = input.nativeElement;

      expect(el.value).toBe('');

      el.value = 'someValue';
      el.dispatchEvent(new Event('input'));

      expect(fixture.componentInstance.outlet.name).toBe('someValue');
      expect(el.className).toContain('ng-touched');
      expect(el.className).toContain('ng-valid');
      expect(el.className).toContain('ng-dirty');
    })
  });

  it('ngOnInit should put correct data into brands', () => {
    component.ngOnInit();
    customerService.getAllCustomers.and.returnValue(mockBrands);
    fixture.detectChanges();
    expect(component.brands).toBe(mockBrands);
  });

  it('the lenght of the dropdown menu should be as many as the brands', async () => {

    const trigger = fixture.debugElement.query(By.css('.mat-select')).nativeElement;
    trigger.click();
    fixture.detectChanges();
    await fixture.whenStable().then(() => {
      const inquiryOptions = fixture.debugElement.queryAll(By.css('.mat-option'));
      expect(inquiryOptions.length).toEqual(mockBrands.length);
    });
  });

  it('the default customer brand should be empty/undefined', async () => {

    const trigger = fixture.debugElement.query(By.css('.mat-select')).nativeElement;
    trigger.click();
    fixture.detectChanges();
    await fixture.whenStable().then(() => {
      const value = trigger.value;
      expect(value).toBe(undefined);
    });
  });

  it('when isComponentInCreateForm false, getOutlet data should be received', () => {
    component.isComponentInCreateState = false;
    component.ngOnInit();
    component.putOutletDetailsIntoUpdateForm();
    expect(component.outlet).toBe(mockOutlet);
  })

  it('when component is in UpdateForm, outlet name should be already filled', () => {
    component.isComponentInCreateState = false;
    component.ngOnInit();

    fixture.whenStable().then(() => {
      const input = fixture.debugElement.query(By.css('input'));
      const el = input.nativeElement;

      expect(el.value).toBe(fixture.componentInstance.outlet.name);
    })
  })

});
