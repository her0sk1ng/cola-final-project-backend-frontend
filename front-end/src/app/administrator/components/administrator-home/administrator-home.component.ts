import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-administrator-home',
  templateUrl: './administrator-home.component.html',
  styleUrls: ['./administrator-home.component.scss'],
})
export class AdministratorHomeComponent implements OnInit {
  public navLinks: any[];
  public activeLinkIndex = 0;
  public showNav$: Observable<boolean>;
  constructor(private router: Router, private authService: AuthService) { 

  }

  public ngOnInit() {
    this.showNav$ = this.authService.role$.pipe(
      map(role => role === 'admin')
    );
    this.pushNavigationLinks();
  }

  public pushNavigationLinks() {
    this.navLinks = [
      {
        label: 'Users',
        link: '/administrator/users',
        index: 0,
        icon: 'account_circle',
      },
      {
        label: 'Outlets',
        link: '/administrator/outlets',
        index: 1,
        icon: 'shopping_cart',
      },
      {
        label: 'Customers',
        link: '/administrator/customers',
        index: 2,
        icon: 'business_center',
      },
      {
        label: 'User Activities',
        link: '/administrator/userActivities',
        index: 3,
        icon: 'transfer_within_a_station',
      },
      {
        label: 'Reports',
        link: '/administrator/reports',
        index: 4,
        icon: 'attachment',
      },
      {
        label: 'Dashboard',
        link: '/administrator/dashboard',
        index: 5,
        icon: 'dashboard',
      },
    ];

    // this.router.events.subscribe(res => {
    //   this.activeLinkIndex = this.navLinks.indexOf(
    //     this.navLinks.find(tab => tab.link === '.' + this.router.url)
    //   );
    // });
  }
}
