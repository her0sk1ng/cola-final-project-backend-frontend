import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from '../auth/admin.guard';
import { AdministratorCustomersCreateComponent } from './components/administrator-customers-create/administrator-customers-create.component';
import { AdministratorCustomersComponent } from './components/administrator-customers/administrator-customers.component';
import { AdministratorDashboardComponent } from './components/administrator-dashboard/administrator-dashboard.component';
import { AdministratorHomeComponent } from './components/administrator-home/administrator-home.component';
import { AdministratorLoginComponent } from './components/administrator-login/administrator-login.component';
import { AdministratorOutletsCreateComponent } from './components/administrator-outlets-create/administrator-outlets-create.component';
import { AdministratorOutletsComponent } from './components/administrator-outlets/administrator-outlets.component';
import { AdministratorReportsComponent } from './components/administrator-reports/administrator-reports.component';
import { AdministratorUserActivitiesComponent } from './components/administrator-user-activities/administrator-user-activities.component';
import { AdministratorUsersCreateComponent } from './components/administrator-users-create/administrator-users-create.component';
import { AdministratorUsersComponent } from './components/administrator-users/administrator-users.component';

const routes: Routes = [

  {
    path: '',
    component: AdministratorHomeComponent,
    children: [
      { path: '', component: AdministratorLoginComponent, pathMatch: 'full' },
      // {
      //   path: 'home',
      //   component: AdministratorHomeComponent,
      //   canActivate: [AdminGuard],
      // },
      {
        path: 'users',
        component: AdministratorUsersComponent,
        canActivate: [AdminGuard],
      },
      {
        path: 'users/:id',
        component: AdministratorUsersCreateComponent,
        canActivate: [AdminGuard],
      },
      {
        path: 'userscreate',
        component: AdministratorUsersCreateComponent,
        canActivate: [AdminGuard],
      },
      {
        path: 'outlets',
        component: AdministratorOutletsComponent,
        canActivate: [AdminGuard],
      },
      {
        path: 'outlets/:id',
        component: AdministratorOutletsCreateComponent,
        canActivate: [AdminGuard],
      },
      {
        path: 'outletscreate',
        component: AdministratorOutletsCreateComponent,
        canActivate: [AdminGuard],
      },
      {
        path: 'customers',
        component: AdministratorCustomersComponent,
        canActivate: [AdminGuard],
      },
      {
        path: 'customers/:id',
        component: AdministratorCustomersCreateComponent,
        canActivate: [AdminGuard],
      },
      {
        path: 'customerscreate',
        component: AdministratorCustomersCreateComponent,
        canActivate: [AdminGuard],
      },
      {
        path: 'reports',
        component: AdministratorReportsComponent,
        canActivate: [AdminGuard],
      },
      {
        path: 'dashboard',
        component: AdministratorDashboardComponent,
        canActivate: [AdminGuard],
      },
      {
        path: 'userActivities',
        component: AdministratorUserActivitiesComponent,
        canActivate: [AdminGuard],
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdministratorRoutingModule { }
