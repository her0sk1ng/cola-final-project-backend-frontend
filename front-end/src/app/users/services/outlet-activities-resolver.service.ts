import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { mergeMap } from 'rxjs/operators';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { UsersService } from 'src/app/core/services/users.service';
import { RedemptionActivitiesService } from './redemption-activities.service';

@Injectable({
  providedIn: 'root',
})
export class OutletActivitiesResolverService {
  private outlet = '';

  constructor(
    private readonly redemptionActivitiesService: RedemptionActivitiesService,
    private readonly notificator: NotificatorService,
    private usersService: UsersService
  ) { }

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.usersService.getUser(route.params.id).pipe(
      mergeMap(user => {
        return this.redemptionActivitiesService
          .getOutletRedemptionActivities(user.outlet)
          .pipe(
            catchError(res => {
              this.notificator.error(res.error.error);
              return of({ outletActivities: [] });
            })
          );
      })
    );
  }
}
