import { TestBed } from '@angular/core/testing';

import { OutletActivitiesResolverService } from './outlet-activities-resolver.service';

describe('OutletActivitiesResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OutletActivitiesResolverService = TestBed.get(
      OutletActivitiesResolverService
    );
    expect(service).toBeTruthy();
  });
});
