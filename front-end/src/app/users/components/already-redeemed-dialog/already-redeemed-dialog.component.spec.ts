import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlreadyRedeemedDialogComponent } from './already-redeemed-dialog.component';

describe('AlreadyRedeemedDialogComponent', () => {
  let component: AlreadyRedeemedDialogComponent;
  let fixture: ComponentFixture<AlreadyRedeemedDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AlreadyRedeemedDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlreadyRedeemedDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
