import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-already-redeemed-dialog',
  templateUrl: './already-redeemed-dialog.component.html',
  styleUrls: ['./already-redeemed-dialog.component.scss'],
})
export class AlreadyRedeemedDialogComponent implements OnInit {
  public redeemedCodeCustomer: string;
  public redeemedCodeOutlet: string;
  public redeemedCodeTimeStamp: Date;
  public redeemedCodeUserName: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Observable<any>) { }

  public ngOnInit() {
    this.data.subscribe(data => {
      this.redeemedCodeCustomer = data.customer;
      this.redeemedCodeOutlet = data.outlet;
      this.redeemedCodeTimeStamp = data.timestamp;
      this.redeemedCodeUserName = data.username;
    });
  }
}
