import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { BarcodeFormat } from '@zxing/library';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.scss'],
})
export class ScannerComponent implements OnInit {
  public barcodeForm: FormGroup;
  public availableDevices: MediaDeviceInfo[];
  public currentDevice: MediaDeviceInfo = null;

  public formatsEnabled: BarcodeFormat[] = [
    BarcodeFormat.EAN_13,
    BarcodeFormat.QR_CODE,
  ];

  public hasDevices: boolean;
  public hasPermission: boolean;

  public qrResultString: string;

  public torchEnabled = false;
  public torchAvailable$ = new BehaviorSubject<boolean>(false);
  public tryHarder = false;

  public sound = new Audio('assets/barcode.wav');

  constructor(
    private readonly dialog: MatDialog,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router
  ) {}

  public ngOnInit() {
    this.barcodeForm = this.formBuilder.group({
      barcodeValue: ['', [Validators.required, Validators.pattern(/^\d{13}$/)]],
    });
  }

  get barcodeValue() {
    return this.barcodeForm.get('barcodeValue');
  }

  public clearResult(): void {
    this.qrResultString = null;
  }

  public onCamerasFound(devices: MediaDeviceInfo[]): void {
    this.availableDevices = devices;
    this.hasDevices = Boolean(devices && devices.length);
  }

  public onCodeResult(resultString: string) {
    this.sound.play();
    this.qrResultString = resultString;
  }

  public onDeviceSelectChange(selected: string) {
    const device = this.availableDevices.find(x => x.deviceId === selected);
    this.currentDevice = device || null;
  }

  public onHasPermission(has: boolean) {
    this.hasPermission = has;
  }

  public onTorchCompatible(isCompatible: boolean): void {
    this.torchAvailable$.next(isCompatible || false);
  }

  public toggleTorch(): void {
    this.torchEnabled = !this.torchEnabled;
  }

  public checkCode(barcodeValue: string) {
    this.router.navigate([`users/code/${barcodeValue}`]);
  }
}
