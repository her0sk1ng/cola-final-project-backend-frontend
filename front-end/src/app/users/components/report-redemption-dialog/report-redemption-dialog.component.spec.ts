import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportRedemptionDialogComponent } from './report-redemption-dialog.component';

describe('ReportRedemptionDialogComponent', () => {
  let component: ReportRedemptionDialogComponent;
  let fixture: ComponentFixture<ReportRedemptionDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReportRedemptionDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportRedemptionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
