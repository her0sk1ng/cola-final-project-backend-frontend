import { Component, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';
import { CodeCheckingService } from 'src/app/core/services/code-checking.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { RedemptionActivitiesService } from '../../services/redemption-activities.service';
import { AlreadyRedeemedDialogComponent } from '../already-redeemed-dialog/already-redeemed-dialog.component';
import { PrizeScanDialogComponent } from '../prize-scan-dialog/prize-scan-dialog.component';
import { ReportRedemptionDialogComponent } from '../report-redemption-dialog/report-redemption-dialog.component';

@Component({
  selector: 'app-check-code',
  templateUrl: './check-code.component.html',
  styleUrls: ['./check-code.component.scss'],
})
export class CheckCodeComponent implements OnInit, OnDestroy {
  public code = '';
  public winning: boolean;
  public userId: string;
  public prize: string;
  public redeemed: boolean;
  public activityRecordId: string;
  private userSubscription: Subscription;
  public reportRedemptionDisabled = false;

  constructor(
    private readonly auth: AuthService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    public readonly dialog: MatDialog,
    private readonly codeChecker: CodeCheckingService,
    private readonly notificator: NotificatorService,
    private readonly redemptionService: RedemptionActivitiesService,
  ) {}

  public ngOnInit() {
    this.route.params.subscribe(data => {
      this.code = data.id;
    });

    this.userSubscription = this.auth.userId$.subscribe(data => {
      this.userId = data;
    });

    this.codeChecker.checkCode(this.code).subscribe(
      data => {
        if (data.prize === 'Not winning!') {
          this.winning = false;
          this.redeemed = false;
        } else {
          this.winning = true;
          this.prize = data.prize;
          this.redeemed = data.redeemed;
        }
        this.redemptionService
          .saveScan(this.code, this.winning, this.redeemed)
          .subscribe(
            activity => (this.activityRecordId = activity.id),
            error =>
              this.notificator.error(
                `Ooops something went wrong! Couldn't save scan record`,
              ),
          );
      },
      error => {
        this.notificator.error(error.message);
        this.router.navigate([`users/profile/${this.userId}`]);
      },
    );
  }

  public ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  public redeem(activityRecordId: string, code: string) {
    this.redemptionService.redeem(activityRecordId, code).subscribe(
      data => {
        this.notificator.success(`Code ${code} was succesfully redeemed`);
        this.router.navigate([`users/profile/${this.userId}`]);
      },
      error => {
        'There was an error trying to redeem this code! Please contact helpdesk!';
      },
    );
  }

  // this dialog is for code prize scan confirmation or redemption
  public openDialog(): void {
    const dialogRef = this.dialog.open(PrizeScanDialogComponent, {
      // width: '250px',
      data: { user: this.userId, prize: this.prize },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.router.navigate([`users/prize/${this.activityRecordId}`]);
      }
      if (result === false) {
        this.redeem(this.activityRecordId, this.code);
      }
    });
  }

  // this is for already redeemed code details
  public openAlreadyRedeemedDialog(): void {
    const request = this.redemptionService.getRedeemedCodeDetails(this.code);
    this.dialog.open(AlreadyRedeemedDialogComponent, { data: request });
  }

  public reportRedemptionAttempt(): void {
    const dialogRef = this.dialog.open(ReportRedemptionDialogComponent, {});
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.redemptionService
          .reportRedemptionAttempt(this.userId, this.code)
          .subscribe(
            data => {
              this.notificator.success(
                `Report Redemption Email was succesfully sent to administrator`,
              ),
                (this.reportRedemptionDisabled = true);
            },
            error =>
              this.notificator.error(
                `Ooops, something went wrong, please report this problem to helpdesk`,
              ),
          );
      }
    });
  }

  public logout() {
    this.auth.logout();
    this.notificator.success(`You have logged out.`);
  }
}
