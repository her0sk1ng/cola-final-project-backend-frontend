import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OutletActivitiesComponent } from './outlet-activities.component';

describe('OutletActivitiesComponent', () => {
  let component: OutletActivitiesComponent;
  let fixture: ComponentFixture<OutletActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutletActivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutletActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
