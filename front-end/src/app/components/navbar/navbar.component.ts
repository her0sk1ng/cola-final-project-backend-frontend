import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  @Input()
  public loggedIn;

  @Input()
  public userId;

  @Input()
  public username;

  @Input()
  public isAdmin;

  @Output()
  public toggle = new EventEmitter<undefined>();

  @Output()
  public logout = new EventEmitter<undefined>();

  constructor() {}

  public ngOnInit() {}

  public toggleSidebar() {
    this.toggle.emit();
  }

  public triggerLogout() {
    this.logout.emit();
  }
}
