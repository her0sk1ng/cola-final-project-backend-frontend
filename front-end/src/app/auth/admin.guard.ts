import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AuthService } from '../core/services/auth.service';
import { NotificatorService } from '../core/services/notificator.service';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivate {
  constructor(
    private readonly authService: AuthService,
    private readonly notificatorService: NotificatorService,
    private readonly router: Router
  ) {}

  public canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.authService.role$.pipe(
      map(role => role === 'admin'),
      tap(role => {
        if (!role) {
          this.notificatorService.error(
            `You're unauthorized to access this page!`
          );
          this.router.navigateByUrl('/home');
        }
      })
    );
  }
}
