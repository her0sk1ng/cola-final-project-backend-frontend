import { TestBed } from '@angular/core/testing';

import { CodeCheckingService } from './code-checking.service';

describe('CodeCheckingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CodeCheckingService = TestBed.get(CodeCheckingService);
    expect(service).toBeTruthy();
  });
});
