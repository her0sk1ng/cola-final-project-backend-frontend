import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { defer, of } from 'rxjs';
import { CreateUser } from 'src/app/common/models/create-user';
import { User } from 'src/app/common/models/user';
import { UsersService } from './users.service';

describe('UsersService', () => {
  const http = jasmine.createSpyObj('HttpClient', [
    'get',
    'post',
    'put',
    'delete',
  ]);
  let httpTestingController: HttpTestingController;

  function asyncError<T>(errorObject: any) {
    return defer(() => Promise.reject(errorObject));
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HttpClientTestingModule,
        HttpTestingController,
        UsersService,
        {
          provide: HttpClient,
          useValue: http,
        },
      ],
    });

    httpTestingController = TestBed.get(HttpTestingController);
  });

  it(`should be created`, () => {
    const service: UsersService = TestBed.get(UsersService);
    expect(service).toBeTruthy();
  });

  it(`getAllUsers shoudl return array with all users`, () => {
    http.get.and.returnValue(
      of([
        {
          id: '54545',
          name: 'BestUser',
        },
        {
          id: '22234',
          name: 'WorstUser',
        },
      ]),
    );

    const service: UsersService = TestBed.get(UsersService);

    http.get.calls.reset();

    service.getAllUsers().subscribe(response => {
      expect(response[0].id).toBe('54545');
      expect(response[0].name).toBe('BestUser');
      expect(response[1].id).toBe('22234');
      expect(response[1].name).toBe('WorstUser');
      expect(http.get).toHaveBeenCalledTimes(1);
    });
  });

  it(`getAllUsers shoudl return error when data is not displayed or error on the server `, () => {
    const errorServerSide = new HttpErrorResponse({
      error: `error`,
      status: 400,
      statusText: `Error Request`,
    });

    http.get.and.returnValue(asyncError(errorServerSide));

    const service: UsersService = TestBed.get(UsersService);

    service
      .getAllUsers()
      .subscribe(
        res => fail(`expected an error`),
        err => expect(err.error).toBe(`error`),
      );
  });

  it(`getUser should get you back one user`, () => {
    const service: UsersService = TestBed.get(UsersService);

    http.get.and.returnValue(
      of({
        id: '222',
        name: 'Sasho',
      }),
    );

    http.get.calls.reset();

    service.getUser('222').subscribe(response => {
      expect(response.id).toBe('222');
      expect(response.name).toBe('Sasho');
      expect(http.get).toHaveBeenCalledTimes(1);
    });
  });

  it(`getUser Should return an error when called when server returns an Error `, () => {
    const errorServerSide = new HttpErrorResponse({
      error: `error`,
      status: 400,
      statusText: `Error Request`,
    });

    http.get.and.returnValue(asyncError(errorServerSide));

    const service: UsersService = TestBed.get(UsersService);

    service
      .getUser('123123')
      .subscribe(
        res => fail(`expected an error`),
        err => expect(err.error).toBe(`error`),
      );
  });

  it(`deleteUser should not return the user when its deleted`, () => {
    const service: UsersService = TestBed.get(UsersService);

    http.delete.and.returnValue(
      of({
        id: '123',
        firstName: 'Tralala',
      }),
    );

    http.delete.calls.reset();

    service.deleteUser('123').subscribe(response => {
      expect(response.id).toBe('123');
      expect(response.name).toBe(undefined);
      expect(http.delete).toHaveBeenCalledTimes(1);
    });
  });

  // it(`deleteUser() should return an error when the server returns error`, () => {

  //   const errorResponse = new HttpErrorResponse({
  //     error: `test 400 error`,
  //     status: 400, statusText: `Bad Request`
  //   });

  //   http.post.and.returnValue(asyncError(errorResponse));

  //   const service: UsersService = TestBed.get(UsersService);

  //   service.deleteUser('123').subscribe(
  //     res => fail('expected an error'),
  //     err => expect(err.error).toBe('test 400 error'),
  //   );
  // });

  it(`createUser() should return the created User `, () => {
    const service: UsersService = TestBed.get(UsersService);
    const mockUser: CreateUser = {
      name: 'BestEver',
      email: 'best@abv.bg',
      password: 'bestpassword',
      phone: '123123123',
      outlet: 'TestOutlet',
    };

    http.post.and.returnValue(of(mockUser));

    http.post.calls.reset();

    service.createUser(mockUser).subscribe(response => {
      expect(response.name).toBe('BestEver');
      expect(response.email).toBe('best@abv.bg');
      expect(response.phone).toBe('123123123');
      expect(response.outlet).toBe('TestOutlet');
      expect(http.post).toHaveBeenCalledTimes(1);
    });
  });

  it(`updateUser() should return the updated User`, () => {
    const service: UsersService = TestBed.get(UsersService);

    const mockUser: User = {
      id: '11',
      createdOn: '11',
      version: 1,
      name: 'BestEver',
      email: 'best@abv.bg',
      phone: '123123123',
      outlet: 'TestOutlet',
    };

    const userId = '11';

    const mockUpdateUser: CreateUser = {
      name: 'BestEver22',
      email: 'best@abv.bg22',
      password: 'test',
      phone: '12312312322',
      outlet: 'TestOutlet22',
    };

    http.put.and.returnValue(of(mockUpdateUser));

    http.put.calls.reset();

    service.updateUser(userId, mockUpdateUser).subscribe(response => {
      expect(response.name).toBe('BestEver22');
      expect(response.email).toBe('best@abv.bg22');
      expect(response.phone).toBe('12312312322');
      expect(response.outlet).toBe('TestOutlet22');
      expect(http.put).toHaveBeenCalledTimes(1);
    });
  });

  it(`updateUser() Should return an error when called when server returns an Error `, () => {
    const errorServerSide = new HttpErrorResponse({
      error: `error`,
      status: 400,
      statusText: `Error Request`,
    });

    const id = '11';
    const mockUpdateUser: CreateUser = {
      name: 'BestEver',
      password: 'asdfdsaf',
      email: 'best@abv.bg',
      phone: '123123123',
      outlet: 'TestOutlet',
    };

    http.put.and.returnValue(asyncError(errorServerSide));
    http.put.calls.reset();

    const service: UsersService = TestBed.get(UsersService);

    service
      .updateUser(id, mockUpdateUser)
      .subscribe(
        res => fail(`expected an error`),
        err => expect(err.error).toBe(`error`),
      );
  });

  it(`createUser() Should return an error when called when server returns an Error `, () => {
    const errorServerSide = new HttpErrorResponse({
      error: `error`,
      status: 400,
      statusText: `Error Request`,
    });

    const mockUser: CreateUser = {
      name: 'BestEver',
      password: 'asdfdsaf',
      email: 'best@abv.bg',
      phone: '123123123',
      outlet: 'TestOutlet',
    };

    http.post.and.returnValue(asyncError(errorServerSide));
    http.post.calls.reset();

    const service: UsersService = TestBed.get(UsersService);

    service
      .createUser(mockUser)
      .subscribe(
        res => fail(`expected an error`),
        err => expect(err.error).toBe(`error`),
      );
  });

  it('addLogoutActivity() should add logout activity and return it', () => {
    const service: UsersService = TestBed.get(UsersService);

    const logout = 'logout';
    const mockActivity: User = {
      name: 'test',
      phone: 'test123',
      id: '11',
      createdOn: '11',
      outlet: 'test',
      version: 1,
      email: 'asdf@abv.bg',
    };

    http.post.and.returnValue(
      of({
        mockActivity,
        logout,
      }),
    );

    http.post.calls.reset();

    service.addLogoutActivity().subscribe(response => {
      expect(mockActivity.name).toBe('test');
      expect(mockActivity.phone).toBe('test123');
      expect(mockActivity.id).toBe('11');
      expect(mockActivity.createdOn).toBe('11');
      expect(mockActivity.outlet).toBe('test');
      expect(mockActivity.version).toBe(1);
      expect(mockActivity.email).toBe('asdf@abv.bg');
      expect(logout).toBe('logout');
      expect(http.post).toHaveBeenCalledTimes(1);
    });
  });

  it('addLoginActivity() should add login activity and return it', () => {
    const service: UsersService = TestBed.get(UsersService);

    const login = 'login';
    const mockActivity: User = {
      name: 'test',
      phone: 'test123',
      id: '11',
      createdOn: '11',
      outlet: 'test',
      version: 1,
      email: 'asdf@abv.bg',
    };

    http.post.and.returnValue(
      of({
        mockActivity,
        login,
      }),
    );

    http.post.calls.reset();

    service.addLoginActivity().subscribe(response => {
      expect(mockActivity.name).toBe('test');
      expect(mockActivity.phone).toBe('test123');
      expect(mockActivity.id).toBe('11');
      expect(mockActivity.createdOn).toBe('11');
      expect(mockActivity.outlet).toBe('test');
      expect(mockActivity.version).toBe(1);
      expect(mockActivity.email).toBe('asdf@abv.bg');
      expect(login).toBe('login');
      expect(http.post).toHaveBeenCalledTimes(1);
    });
  });

  it(`getAllUserActivities shoudl return array with all useractivities`, () => {
    http.get.and.returnValue(
      of([
        {
          id: '54545',
          description: 'nice',
          date: '11',
          customer: 'bestbrand',
          outlet: 'bestoutlet',
          visibleUser: 'BestUser',
        },
        {
          id: '11',
          description: 'nice22',
          date: '11',
          customer: 'bestbrand1',
          outlet: 'bestoutlet1',
          visibleUser: 'BestUser1',
        },
      ]),
    );

    const service: UsersService = TestBed.get(UsersService);

    http.get.calls.reset();

    service.getAllUserActivities().subscribe(response => {
      expect(response[0].id).toBe('54545');
      expect(response[0].description).toBe('nice');
      expect(response[0].date).toBe('11');
      expect(response[0].customer).toBe('bestbrand');
      expect(response[0].outlet).toBe('bestoutlet');
      expect(response[0].visibleUser).toBe('BestUser');
      expect(response[1].id).toBe('11');
      expect(response[1].description).toBe('nice22');
      expect(response[1].date).toBe('11');
      expect(response[1].customer).toBe('bestbrand1');
      expect(response[1].outlet).toBe('bestoutlet1');
      expect(response[1].visibleUser).toBe('BestUser1');
      expect(http.get).toHaveBeenCalledTimes(1);
    });
  });

  it(`getAllUsersActivites shoudl return error when data is not displayed or error on the server `, () => {
    const errorServerSide = new HttpErrorResponse({
      error: `error`,
      status: 400,
      statusText: `Error Request`,
    });

    http.get.and.returnValue(asyncError(errorServerSide));

    const service: UsersService = TestBed.get(UsersService);

    http.get.calls.reset();

    service
      .getAllUserActivities()
      .subscribe(
        res => fail(`expected an error`),
        err => expect(err.error).toBe(`error`),
      );
  });
});
