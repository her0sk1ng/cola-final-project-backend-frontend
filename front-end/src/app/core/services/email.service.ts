import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Email } from '../../common/models/email';
import { StatusResult } from '../../common/models/status-result';

@Injectable({
  providedIn: 'root',
})
export class EmailService {
  constructor(private readonly http: HttpClient) {}

  public sendEmail(email: Email): Observable<Email> {
    return this.http.post<Email>(`http://localhost:3000/api/email`, email);
  }
}
