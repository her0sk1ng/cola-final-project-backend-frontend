import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateOutlet } from '../../../app/common/models/create-outlet';
import { Outlet } from '../../../app/common/models/outlet';

@Injectable({
  providedIn: 'root',
})
export class OutletService {
  public username = localStorage.getItem('outlet');
  constructor(private readonly http: HttpClient) {}

  public getAllOutlets(search?: string): Observable<Outlet[]> {
    return this.http.get<Outlet[]>(`http://localhost:3000/api/outlets`);
  }

  public getOutlet(id: string): Observable<Outlet> {
    return this.http.get<Outlet>(`http://localhost:3000/api/outlets/${id}`);
  }

  public createOutlet(outlet: CreateOutlet): Observable<Outlet> {
    return this.http.post<Outlet>(`http://localhost:3000/api/outlets`, outlet);
  }

  public deleteOutlet(id: string): Observable<Outlet> {
    return this.http.delete<Outlet>(`http://localhost:3000/api/outlets/${id}`);
  }

  public updateOutlet(id: string, outlet: CreateOutlet): Observable<Outlet> {
    return this.http.put<Outlet>(
      `http://localhost:3000/api/outlets/${id}`,
      outlet
    );
  }

  // public getAllUsers(search?: string): Observable<User[]>{
  //   return this.http.get<User[]>('http://localhost:3000/api/users')
  // }

  // public getUser(id: string): Observable<User>{
  //   return this.http.get<User>(`http://localhost:3000/api/users/${id}`)
  // }

  // public deleteUser(id: string): Observable<User>{
  //   return this.http.delete<User>(`http://localhost:3000/api/users/${id}`)
  // }

  // public updateUser(id: string, user: CreateUser): Observable<User> {
  //   return this.http.put<User>(`http://localhost:3000/api/users/${id}`, user);
  // }

  // public createUser(user: CreateUser): Observable<User> {
  //   return this.http.post<User>(`http://localhost:3000/api/users`, user)
  // }
}
