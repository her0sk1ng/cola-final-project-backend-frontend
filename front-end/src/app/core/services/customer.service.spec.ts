import { TestBed } from '@angular/core/testing';
import { CustomerService } from './customer.service';
import { defer, of } from 'rxjs';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Customer } from 'src/app/common/models/customer';
import { CreateCustomer } from 'src/app/common/models/create-customer';

describe('CustomerService', () => {
  const http = jasmine.createSpyObj('HttpClient', [
    'get',
    'post',
    'put',
    'delete',
  ]);

  function asyncError<T>(errorObject: any) {
    return defer(() => Promise.reject(errorObject));
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HttpClient,
          useValue: http,
        },
      ],
    });
  });

  it('should be created', () => {
    const service: CustomerService = TestBed.get(CustomerService);
    expect(service).toBeTruthy();
  });

  it('getAllCustomers should return array with all customers and be called once', () => {
    http.get.and.returnValue(
      of([
        {
          id: '123',
          name: 'bestCustomer',
        },
        {
          id: '234',
          name: 'customer',
        },
      ]),
    );

    const service: CustomerService = TestBed.get(CustomerService);

    http.get.calls.reset();

    service.getAllCustomers().subscribe(response => {
      expect(response[0].id).toBe('123');
      expect(response[0].name).toBe('bestCustomer');
      expect(response[1].id).toBe('234');
      expect(response[1].name).toBe('customer');
      expect(http.get).toHaveBeenCalledTimes(1);
    });
  });

  it(`getAllCustomers shoudl return error when data is not displayed or error on the server `, () => {
    const errorServerSide = new HttpErrorResponse({
      error: `error`,
      status: 400,
      statusText: `Error Request`,
    });

    http.get.and.returnValue(asyncError(errorServerSide));

    const service: CustomerService = TestBed.get(CustomerService);

    service
      .getAllCustomers()
      .subscribe(
        res => fail(`expected an error`),
        err => expect(err.error).toBe(`error`),
      );
  });

  it('getCustomer should return the correct customer and be called once', () => {
    let customer: Customer = {
      id: '11',
      name: 'test',
    };

    http.get.and.returnValue(of(customer));

    http.get.calls.reset();

    const service: CustomerService = TestBed.get(CustomerService);

    service.getCustomer('123').subscribe(response => {
      expect(response.id).toBe('11');
      expect(response.name).toBe('test');
      expect(http.get).toHaveBeenCalledTimes(1);
    });
  });

  it(`getCustomer shoudl return error when data is not displayed or error on the server `, () => {
    const errorServerSide = new HttpErrorResponse({
      error: `error`,
      status: 400,
      statusText: `Error Request`,
    });

    http.get.and.returnValue(asyncError(errorServerSide));

    const service: CustomerService = TestBed.get(CustomerService);

    service
      .getCustomer('111')
      .subscribe(
        res => fail(`expected an error`),
        err => expect(err.error).toBe(`error`),
      );
  });

  it('createCustoemr should return the created Customer and be called once', () => {
    const service: CustomerService = TestBed.get(CustomerService);

    const mockCreateCustomer: CreateCustomer = {
      name: 'best',
    };

    http.post.and.returnValue(of(mockCreateCustomer));

    http.post.calls.reset();

    service.createCustomer(mockCreateCustomer).subscribe(response => {
      expect(response.name).toBe('best');
      expect(http.post).toHaveBeenCalledTimes(1);
    });
  });

  it(`createCustomer() shoudl return error when data is not displayed or error on the server `, () => {
    const errorServerSide = new HttpErrorResponse({
      error: `error`,
      status: 400,
      statusText: `Error Request`,
    });

    const mockCreateCustomer: CreateCustomer = {
      name: 'best',
    };

    http.post.and.returnValue(asyncError(errorServerSide));

    const service: CustomerService = TestBed.get(CustomerService);

    service
      .createCustomer(mockCreateCustomer)
      .subscribe(
        res => fail(`expected an error`),
        err => expect(err.error).toBe(`error`),
      );
  });

  // CHECK THE BELOW TEST

  it('deleteCustomer() should not return the user when its deleted and be called once', () => {
    const service: CustomerService = TestBed.get(CustomerService);

    http.delete.and.returnValue(
      of({
        id: '11',
        name: 'best',
      }),
    );

    http.delete.calls.reset();

    service.deleteCustomer('11').subscribe(response => {
      expect(response.id).toBe('11');
      expect(response.name).toBe('best');
      expect(http.delete).toHaveBeenCalledTimes(1);
    });
  });

  it(`createCustomer() shoudl return error when data is not displayed or error on the server `, () => {
    const errorServerSide = new HttpErrorResponse({
      error: `error`,
      status: 400,
      statusText: `Error Request`,
    });

    http.delete.and.returnValue(asyncError(errorServerSide));

    const service: CustomerService = TestBed.get(CustomerService);

    service
      .deleteCustomer('11')
      .subscribe(
        res => fail(`expected an error`),
        err => expect(err.error).toBe(`error`),
      );
  });

  it('updateCustomer() should return the updatedCustoemr and be called once', () => {
    const service: CustomerService = TestBed.get(CustomerService);
    const mockId = '11';

    const mockCreateCustomer: CreateCustomer = {
      name: 'best',
    };

    http.put.and.returnValue(of(mockCreateCustomer));

    http.put.calls.reset();

    service.updateCustomer(mockId, mockCreateCustomer).subscribe(response => {
      expect(response.name).toBe('best');
      expect(http.put).toHaveBeenCalledTimes(1);
    });
  });

  it(`updateCustomer() shoudl return error when data is not displayed or error on the server `, () => {
    const errorServerSide = new HttpErrorResponse({
      error: `error`,
      status: 400,
      statusText: `Error Request`,
    });

    const mockUpdate = { name: 'asd' };
    const mockId = '11';

    http.put.and.returnValue(asyncError(errorServerSide));

    const service: CustomerService = TestBed.get(CustomerService);

    service
      .updateCustomer(mockId, mockUpdate)
      .subscribe(
        res => fail(`expected an error`),
        err => expect(err.error).toBe(`error`),
      );
  });
});
