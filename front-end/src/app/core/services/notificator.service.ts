import { Injectable } from '@angular/core';
// import * as toastr from 'toastr';
import { ToastrService } from 'ngx-toastr';

@Injectable({ providedIn: 'root' })
export class NotificatorService {
  constructor(private readonly toastr: ToastrService) {}

  public success(message: string, title?: string): void {
    this.toastr.success(message, title);
  }

  public error(message: string, title?: string): void {
    this.toastr.warning(message, title);
  }
}
