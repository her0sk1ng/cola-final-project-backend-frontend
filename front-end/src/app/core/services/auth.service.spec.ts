import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { defer, of } from 'rxjs';
import { AuthService } from './auth.service';
import { StorageService } from './storage.service';

describe('AuthService', () => {
  // dependencies mock

  const http = jasmine.createSpyObj('HttpClient', ['get', 'post']);
  const storage = jasmine.createSpyObj('StorageService', [
    'getItem',
    'setItem',
    'removeItem',
  ]);

  // this is used to errors from http
  function asyncError<T>(errorObject: any) {
    return defer(() => Promise.reject(errorObject));
  }

  // set dependencies to our mock implementation
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HttpClient,
          useValue: http,
        },
        {
          provide: StorageService,
          useValue: storage,
        },
      ],
    }),
  );

  // test scenario 1
  it(`should be created`, () => {
    const service: AuthService = TestBed.get(AuthService);
    expect(service).toBeTruthy();
  });

  // test scenario 2
  it(`login should log the user in`, () => {
    // mock the return value of http.post
    http.post.and.returnValue(
      of({
        token: 'token',
        user: {
          name: 'test',
          role: 'admin',
        },
      }),
    );

    // call the login method and check if result matches the mock return (act/assert)
    const service: AuthService = TestBed.get(AuthService);
    service.login('email', 'password').subscribe(response => {
      expect(response.user.name).toBe('test');
      expect(response.token).toBe('token');
      expect(response.user.role).toBe('admin');
    });
  });

  // IS IT CALLING IT TWICE BECAUSE THERE ARE 2 LOGINS?? YES?
  // it(`login should call http.post`, () => {
  //   const service: AuthService = TestBed.get(AuthService);

  //   http.post.calls.reset();

  //   service.login('email', 'password').subscribe(
  //     () => expect(http.post).toHaveBeenCalledTimes(1)
  //   );
  // });
  //   service
  //     .login('email', 'password')
  //     .subscribe(() => expect(http.post).toHaveBeenCalledTimes(1));
  // });

  it(`login should call storage.set three times`, () => {
    const service: AuthService = TestBed.get(AuthService);

    storage.setItem.calls.reset();

    service
      .login('email', 'password')
      .subscribe(() => expect(storage.setItem).toHaveBeenCalledTimes(4));
  });

  it(`login should update the user subject`, () => {
    http.post.and.returnValue(
      of({
        token: 'token',
        user: {
          name: 'test',
        },
      }),
    );

    const service: AuthService = TestBed.get(AuthService);
    service.login('email', 'password').subscribe(() => {
      service.user$.subscribe(username => expect(username).toBe('test'));
    });
  });

  it(`logout should change the user subject to <null>`, () => {
    const service: AuthService = TestBed.get(AuthService);

    service.logout();

    service.user$.subscribe(username => expect(username).toBe(null));
  });

  it(`logout should call storage.remove twice`, () => {
    const service: AuthService = TestBed.get(AuthService);

    storage.removeItem.calls.reset();

    service.logout();

    expect(storage.removeItem).toHaveBeenCalledTimes(4);
    expect(storage.removeItem).toHaveBeenCalledWith('token');
    expect(storage.removeItem).toHaveBeenCalledWith('username');
    expect(storage.removeItem).toHaveBeenCalledWith('role');
    expect(storage.removeItem).toHaveBeenCalledWith('id');
  });

  it(`register should call http.post`, () => {
    const service: AuthService = TestBed.get(AuthService);

    http.post.calls.reset();
    service
      .register('firstName', 'lastName', 'username', 'email', 'password')
      .subscribe(() => expect(http.post).toHaveBeenCalledTimes(1));
  });

  it(`register should create new user`, () => {
    const expectedUser = {
      id: 'testId',
      firstName: 'Genadi',
      lastName: 'Genadiev',
      userName: 'genadi',
      email: 'genadi@realgenadi.com',
    };
    // mock the return value of http.post
    http.post.and.returnValue(of(expectedUser));

    // call the login method and check if result matches the mock return (act/assert)
    const service: AuthService = TestBed.get(AuthService);
    service
      .register('firstName', 'lastName', 'username', 'email', 'password')
      .subscribe(response => {
        expect(response).toBe(expectedUser);
      });
  });

  it(`register should return an error when the server returns a 400`, () => {
    const errorResponse = new HttpErrorResponse({
      error: `test 400 error`,
      status: 400,
      statusText: `Bad Request`,
    });

    http.post.and.returnValue(asyncError(errorResponse));

    const service: AuthService = TestBed.get(AuthService);
    service
      .register('firstName', 'lastName', 'username', 'email', 'password')
      .subscribe(
        res => fail('expected an error'),
        err => expect(err.error).toBe('test 400 error'),
      );
  });
});
