export interface Email {
  sender: string;
  message: string;
  subject: string;
}
