export interface Customer {
  id: string;
  name: string;
  numberOfOutlets: number;
  numberOfEmployeesOfCurrentCustomer: number;
}
