export interface RedemptionLog {
  activityRecordId: string;
  code: string;
}
