export interface Outlet {
  id: string;
  name: string;
  newBrand: string;
  numberOfEmployees: string;
}
