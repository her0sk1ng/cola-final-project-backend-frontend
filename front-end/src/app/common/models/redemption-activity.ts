export class RedemptionActivity {
  public id: string;
  public timestamp: Date;
  public code: string;
  public outlet: string;
  public customer: string;
  public redemptionStatus: string;
  public prizeCode: string;
  public username: string;
}
