export interface UserActivities {
  id: string;
  description: string;
  date: string;
  customer: string;
  outlet: string;
  visibleUser: string;
}
