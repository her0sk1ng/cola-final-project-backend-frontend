import 'reflect-metadata';
import { createConnection } from 'typeorm';
import { User } from './data/entities/user';

createConnection()
  // tslint:disable-next-line: no-empty
  .then(async connection => {})
  // tslint:disable-next-line: no-console
  .catch(error => console.log(error));
