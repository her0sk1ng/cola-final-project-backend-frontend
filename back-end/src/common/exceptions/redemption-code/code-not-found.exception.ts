import { BaseException } from '../base.exception';

export class CodeNotFound extends BaseException {}
