import { BaseException } from '../base.exception';

export class OutletNotFoundException extends BaseException {}
