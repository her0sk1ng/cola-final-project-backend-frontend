import { BaseException } from '../base.exception';

export class CustomerAlreadyExists extends BaseException {}
