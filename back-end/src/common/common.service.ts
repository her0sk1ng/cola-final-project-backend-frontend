import { Injectable, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../data/entities/user';
import { Repository, Admin } from 'typeorm';
import { Administrator } from '../data/entities/administrator';
import { UserNotFoundException } from './exceptions/user/user-not-found.exception';
import * as bcrypt from 'bcrypt';
import { UserLoginDTO } from '../models/user/user-login-dto';
import { Outlet } from '../data/entities/outlet';
import { Customer } from '../data/entities/customer';

@Injectable()
export class CommonService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Administrator)
    private readonly adminRepository: Repository<Administrator>,
    @InjectRepository(Outlet)
    private readonly outletRepository: Repository<Outlet>,
    @InjectRepository(Customer)
    private readonly customerRepository: Repository<Customer>,
  ) {}

  public async findUserByEmail(email: string): Promise<User> | undefined {
    const user = await this.userRepository.findOne({ email });
    return user;
  }

  public async findAdminByEmail(
    email: string,
  ): Promise<Administrator> | undefined {
    const user = await this.adminRepository.findOne({ email });
    return user;
  }

  public async findCustomerByName(name: string): Promise<Customer> | undefined {
    const customer = await this.customerRepository.findOne({ name });
    return customer;
  }

  public async findCustomerById(id: string): Promise<Customer> | undefined {
    const customer = await this.customerRepository.findOne({ id });
    return customer;
  }

  public async findUserById(id: string): Promise<User> | undefined {
    const result = await this.userRepository.findOne({ id });

    if (!result) {
      throw new UserNotFoundException(
        `User with id: ${id}, is not found!`,
        HttpStatus.NOT_FOUND,
      );
    }

    return result;
  }

  public async findOutletById(id: string): Promise<Outlet> | undefined {
    const outlet = await this.outletRepository.findOne({ id });
    return outlet;
  }

  public async hashPassword(password): Promise<string> {
    return await bcrypt.hash(password, 10);
  }

  public async valiteUserPass(user, userEntity): Promise<boolean> {
    return await bcrypt.compare(user.password, userEntity.password);
  }

  public async findOutletByName(name: string): Promise<Outlet> | undefined {
    const outlet = await this.outletRepository.findOne({ name });
    return outlet;
  }
}
