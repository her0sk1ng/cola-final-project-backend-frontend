import { Module, ReflectMetadata } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../data/entities/user';
import { UsersService } from './user.service';
import { AuthService } from '../auth/auth.service';
import { UsersController } from './users.controller';
import { CommonModule } from '../common/common.module';
import { PassportModule } from '@nestjs/passport';
import { Customer } from '../data/entities/customer';
import { Outlet } from '../data/entities/outlet';
import { RedemptionActivityController } from './redemption-activity/redemption-activity.controller';
import { WinningCode } from '../data/entities/winning-codes';
import { Prize } from '../data/entities/prizes';
import { RedemptionActivityService } from './redemption-activity/redemption-activity.service';
import { RedemptionActivity } from '../data/entities/redemption-activity';
import { AppService } from '../app.service';
import { AppModule } from '../app.module';
import { SharedModule } from '../shared/shared.module';
import { UserActivity } from '../data/entities/user-activity';
import { AdminActivity } from '../data/entities/admin-activity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      Customer,
      Outlet,
      Prize,
      WinningCode,
      RedemptionActivity,
      UserActivity,
      AdminActivity,
    ]),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    CommonModule,
    SharedModule,
  ],
  providers: [UsersService, RedemptionActivityService],
  exports: [UsersService, RedemptionActivityService],
  controllers: [UsersController, RedemptionActivityController],
})
export class UserModule {}
