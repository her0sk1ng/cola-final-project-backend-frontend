import {
  Controller,
  UseFilters,
  Post,
  HttpCode,
  HttpStatus,
  Body,
  ValidationPipe,
  UseGuards,
  Put,
  Param,
  Delete,
  Get,
} from '@nestjs/common';
import { UserExceptionFilter } from '../common/filters/user-exception-filter';
import { UsersService } from './user.service';
import { UserLoginDTO } from '../models/user/user-login-dto';
import { UserRegisterDTO } from '../models/user/user-register-dto';
import { ShowUserDTO } from '../models/user/show-user.dto';
import { Roles } from '../common/decorators/roles.decorator';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../common/guards/roles.guard';
import { CustomerRegisterDTO } from '../models/customer/customer-register-dto';
import { ShowCustomerDTO } from '../models/customer/show-customer-dto';
import { OutletRegisterDTO } from '../models/outlet/outlet-register-dto';
import { ShowOutletDTO } from '../models/outlet/show-outlet-dto';
import { ShowRedemptionCodeActivityDTO } from '../models/redemption-code/show-redemption-code-activity';
import { RedemptionActivity } from '../data/entities/redemption-activity';
import { AuthUser } from '../common/decorators/user.decorator';
import { User } from '../data/entities/user';
import { UserActivityDTO } from '../models/user/activity-dto';
import { Administrator } from '../data/entities/administrator';
import { UserActivity } from '../data/entities/user-activity';

@Controller('api/')
@UseFilters(new UserExceptionFilter())
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Roles('admin')
  @Get('users')
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(AuthGuard(), RolesGuard)
  public async getAllUsers(): Promise<any> {
    return await this.usersService.getAllUsers();
  }

  @Get('users/:id')
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(AuthGuard())
  public async getUser(@Param('id') id: string): Promise<ShowUserDTO> {
    return await this.usersService.getUser(id);
  }

  @Roles('admin')
  @Get('outlets')
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(AuthGuard(), RolesGuard)
  public async getAllOutlets(
    @Param('id') id: string,
  ): Promise<ShowOutletDTO[]> {
    return await this.usersService.getAllOutlets();
  }

  @Roles('admin')
  @Get('outlets/:id')
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(AuthGuard(), RolesGuard)
  public async getOutlet(@Param('id') id: string): Promise<ShowOutletDTO> {
    return await this.usersService.getOutlet(id);
  }

  @Roles('admin')
  @Get('customers')
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(AuthGuard(), RolesGuard)
  public async getAllCustomers(): Promise<ShowCustomerDTO[]> {
    return await this.usersService.getAllCustomers();
  }

  @Roles('admin')
  @Get('customers/:id')
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(AuthGuard(), RolesGuard)
  public async getCustomer(@Param('id') id: string): Promise<ShowCustomerDTO> {
    return await this.usersService.getCustomer(id);
  }

  @Roles('admin')
  @Post('users')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard(), RolesGuard)
  public async createUser(
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    user: UserRegisterDTO,
  ): Promise<ShowUserDTO> {
    return await this.usersService.createUser(user);
  }

  @Roles('admin')
  @Post('customers')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard(), RolesGuard)
  public async createCustomer(
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    customer: CustomerRegisterDTO,
  ): Promise<ShowCustomerDTO> {
    return await this.usersService.createCustomer(customer);
  }

  @Roles('admin')
  @Post('outlets')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard(), RolesGuard)
  public async createOutlet(
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    outlet: OutletRegisterDTO,
  ): Promise<ShowOutletDTO> {
    return await this.usersService.createOutlet(outlet);
  }

  @Roles('admin')
  @Put('users/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard(), RolesGuard)
  public async updateUser(
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    user: Partial<UserRegisterDTO>,
    @Param('id') id: string,
  ): Promise<ShowUserDTO> {
    return await this.usersService.updateUser(id, user);
  }

  @Roles('admin')
  @Put('outlets/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard(), RolesGuard)
  public async updateOutlet(
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    outlet: Partial<OutletRegisterDTO>,
    @Param('id') id: string,
  ): Promise<ShowOutletDTO> {
    return await this.usersService.updateOutlet(id, outlet);
  }

  @Roles('admin')
  @Put('customers/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard(), RolesGuard)
  public async updateCustomer(
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    customer: CustomerRegisterDTO,
    @Param('id') id: string,
  ): Promise<ShowCustomerDTO> {
    return await this.usersService.updateCustomer(id, customer);
  }

  @Roles('admin')
  @Delete('users/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard(), RolesGuard)
  public async deleteUser(
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    user: Partial<UserRegisterDTO>,
    @Param('id') id: string,
  ): Promise<ShowUserDTO> {
    return await this.usersService.deleteUser(id);
  }

  @Roles('admin')
  @Delete('outlets/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard(), RolesGuard)
  public async deleteOutlet(
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    user: Partial<UserRegisterDTO>,
    @Param('id') id: string,
  ): Promise<ShowOutletDTO> {
    return await this.usersService.deleteOutlet(id);
  }

  @Roles('admin')
  @Delete('customers/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard(), RolesGuard)
  public async deleteCustomer(
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    user: Partial<UserRegisterDTO>,
    @Param('id') id: string,
  ): Promise<ShowCustomerDTO> {
    return await this.usersService.deleteCustomer(id);
  }

  @Roles('admin')
  @Get('activity')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard(), RolesGuard)
  public async getCodeActivity(): Promise<RedemptionActivity[]> {
    return await this.usersService.getAllCodesActivity();
  }

  @Post('users/loginActivity')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard())
  public async addLoginActivity(
    @AuthUser() user: User | Administrator,
  ): Promise<UserActivityDTO> {
    return await this.usersService.addLoginActivity(user);
  }

  @Post('users/logoutActivity')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard())
  public async addLogoutActivity(
    @AuthUser() user: User | Administrator,
  ): Promise<UserActivityDTO> {
    return await this.usersService.addLogoutActivity(user);
  }

  @Roles('admin')
  @Get('allUserActivities')
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(AuthGuard(), RolesGuard)
  public async getAllUserActivities(): Promise<UserActivityDTO[]> {
    return await this.usersService.getAllUserActivities();
  }
}
