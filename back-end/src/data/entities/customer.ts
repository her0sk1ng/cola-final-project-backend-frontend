import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { User } from './user';
import { Outlet } from './outlet';

@Entity('customer')
export class Customer {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar', { unique: true })
  name: string;

  @Column({ default: false })
  isDeleted: boolean;

  @OneToMany(type => Outlet, outlet => outlet.brand)
  outlets: Promise<Outlet[]>;
}
