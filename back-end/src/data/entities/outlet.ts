import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  Unique,
} from 'typeorm';
import { User } from './user';
import { Customer } from './customer';

@Entity('outlet')
export class Outlet {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar', { unique: true })
  name: string;

  @Column({ default: false })
  isDeleted: boolean;

  @OneToMany(type => User, user => user.outlet)
  employees: Promise<User[]>;

  @ManyToOne(type => Customer, customer => customer.outlets)
  brand: Promise<Customer>;
}
