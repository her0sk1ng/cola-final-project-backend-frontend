import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { WinningCode } from './winning-codes';

@Entity('prizes')
export class Prize {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar', { unique: true })
  description: string;

  @OneToMany(type => WinningCode, winningCodes => winningCodes.prize)
  winningCodes: Promise<WinningCode[]>;
}
