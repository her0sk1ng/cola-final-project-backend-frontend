import {
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  ManyToOne,
  Entity,
} from 'typeorm';
import { User } from './user';

@Entity('activities')
export class UserActivity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  description: string;

  @Column('nvarchar')
  outlet: string;

  @Column('nvarchar')
  customer: string;

  @CreateDateColumn()
  date: Date;

  @ManyToOne(type => User, user => user.activities)
  user: Promise<User>;
}
