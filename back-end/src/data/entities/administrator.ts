import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  OneToMany,
} from 'typeorm';
import { AdminActivity } from './admin-activity';

@Entity('administrator')
export class Administrator {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  name: string;

  @Column('nvarchar')
  password: string;

  @Column({ unique: true })
  email: string;

  @CreateDateColumn()
  createdOn: Date;

  @Column({ default: 'admin' })
  priviliges: string;

  @Column({ default: false })
  isDeleted: boolean;

  @OneToMany(type => AdminActivity, activity => activity.admin)
  activities: Promise<AdminActivity[]>;
}
