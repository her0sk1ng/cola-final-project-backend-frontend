import { MigrationInterface, QueryRunner } from 'typeorm';

// tslint:disable-next-line: class-name
export class addedAdminActivity1563453601659 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      // tslint:disable: max-line-length
      'CREATE TABLE `administrator` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `priviliges` varchar(255) NOT NULL DEFAULT \'admin\', `isDeleted` tinyint NOT NULL DEFAULT 0, UNIQUE INDEX `IDX_be0ce9bef56d5a30b9e5752564` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      'CREATE TABLE `activities` (`id` varchar(36) NOT NULL, `description` varchar(255) NOT NULL, `date` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `userId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      'CREATE TABLE `redemption_activity` (`id` varchar(36) NOT NULL, `timestamp` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `code` varchar(255) NOT NULL, `outlet` varchar(255) NOT NULL, `customer` varchar(255) NOT NULL, `redemptionStatus` enum (\'successful\', \'redeemed\', \'cancelled\', \'invalid\', \'scanned\') NOT NULL, `prizeCode` varchar(255) NOT NULL, `userId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      'CREATE TABLE `users` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `priviliges` varchar(255) NOT NULL DEFAULT \'user\', `isDeleted` tinyint NOT NULL DEFAULT 0, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `version` int NOT NULL, `phone` varchar(255) NOT NULL, `outletId` varchar(36) NULL, UNIQUE INDEX `IDX_97672ac88f789774dd47f7c8be` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      'CREATE TABLE `outlet` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `brandId` varchar(36) NULL, UNIQUE INDEX `IDX_6e932841d15452c92572cce298` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      'CREATE TABLE `customer` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, UNIQUE INDEX `IDX_ac1455877a69957f7466d5dc78` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      'CREATE TABLE `winning_codes` (`id` varchar(36) NOT NULL, `barcode` varchar(255) NOT NULL, `redeemed` tinyint NOT NULL DEFAULT 0, `prizeId` varchar(36) NULL, UNIQUE INDEX `IDX_c06f440140e3968b34430764fd` (`barcode`), PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      'CREATE TABLE `prizes` (`id` varchar(36) NOT NULL, `description` varchar(255) NOT NULL, UNIQUE INDEX `IDX_42145916dc4d1a9237ebd68df4` (`description`), PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      'ALTER TABLE `admin_activity` ADD CONSTRAINT `FK_35dd47359c53fee1df9adaa776f` FOREIGN KEY (`adminId`) REFERENCES `administrator`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `activities` ADD CONSTRAINT `FK_5a2cfe6f705df945b20c1b22c71` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `redemption_activity` ADD CONSTRAINT `FK_83b85d8c2347cbadcee920878a2` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `users` ADD CONSTRAINT `FK_44557630e787a2fc078722b3351` FOREIGN KEY (`outletId`) REFERENCES `outlet`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `outlet` ADD CONSTRAINT `FK_da0a7931943e274c1f026c48729` FOREIGN KEY (`brandId`) REFERENCES `customer`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `winning_codes` ADD CONSTRAINT `FK_5742dbe35f445cfe5e1c0891461` FOREIGN KEY (`prizeId`) REFERENCES `prizes`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      'ALTER TABLE `winning_codes` DROP FOREIGN KEY `FK_5742dbe35f445cfe5e1c0891461`',
    );
    await queryRunner.query(
      'ALTER TABLE `outlet` DROP FOREIGN KEY `FK_da0a7931943e274c1f026c48729`',
    );
    await queryRunner.query(
      'ALTER TABLE `users` DROP FOREIGN KEY `FK_44557630e787a2fc078722b3351`',
    );
    await queryRunner.query(
      'ALTER TABLE `redemption_activity` DROP FOREIGN KEY `FK_83b85d8c2347cbadcee920878a2`',
    );
    await queryRunner.query(
      'ALTER TABLE `activities` DROP FOREIGN KEY `FK_5a2cfe6f705df945b20c1b22c71`',
    );
    await queryRunner.query(
      'ALTER TABLE `admin_activity` DROP FOREIGN KEY `FK_35dd47359c53fee1df9adaa776f`',
    );
    await queryRunner.query(
      'DROP INDEX `IDX_42145916dc4d1a9237ebd68df4` ON `prizes`',
    );
    await queryRunner.query('DROP TABLE `prizes`');
    await queryRunner.query(
      'DROP INDEX `IDX_c06f440140e3968b34430764fd` ON `winning_codes`',
    );
    await queryRunner.query('DROP TABLE `winning_codes`');
    await queryRunner.query(
      'DROP INDEX `IDX_ac1455877a69957f7466d5dc78` ON `customer`',
    );
    await queryRunner.query('DROP TABLE `customer`');
    await queryRunner.query(
      'DROP INDEX `IDX_6e932841d15452c92572cce298` ON `outlet`',
    );
    await queryRunner.query('DROP TABLE `outlet`');
    await queryRunner.query(
      'DROP INDEX `IDX_97672ac88f789774dd47f7c8be` ON `users`',
    );
    await queryRunner.query('DROP TABLE `users`');
    await queryRunner.query('DROP TABLE `redemption_activity`');
    await queryRunner.query('DROP TABLE `activities`');
    await queryRunner.query(
      'DROP INDEX `IDX_be0ce9bef56d5a30b9e5752564` ON `administrator`',
    );
    await queryRunner.query('DROP TABLE `administrator`');
  }
}
