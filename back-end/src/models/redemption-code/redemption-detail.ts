import { Expose } from 'class-transformer';
import { OuterSubscriber } from 'rxjs/internal/OuterSubscriber';
import { Timestamp } from 'typeorm';

export class RedemptionDetailsDTO {
  @Expose()
  id: string;

  @Expose()
  outlet: string;

  @Expose()
  customer: string;

  @Expose()
  timestamp: Date;

  @Expose()
  username: string;
}
