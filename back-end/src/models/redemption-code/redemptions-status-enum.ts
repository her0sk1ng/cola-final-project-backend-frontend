export enum RedemptionStatus {
  SUCCESS = 'successful',
  REDEEMED = 'redeemed',
  CANCELLED = 'cancelled',
  INVALID = 'invalid',
  SCANNED = 'scanned',
}
