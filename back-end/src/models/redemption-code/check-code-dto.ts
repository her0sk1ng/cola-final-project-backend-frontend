import { IsString, IsBoolean } from 'class-validator';

export class CheckCodeDTO {
  @IsString()
  code: string;

  @IsBoolean()
  winning: boolean;

  @IsBoolean()
  redeemed: boolean;
}
