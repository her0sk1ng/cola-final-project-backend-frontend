import { Expose } from 'class-transformer';
import { IsEmail, IsString } from 'class-validator';

export class EmailDTO {
  @Expose()
  @IsEmail()
  sender: string;

  @Expose()
  @IsString()
  message: string;

  @Expose()
  @IsString()
  subject: string;
}
