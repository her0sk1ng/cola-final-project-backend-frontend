import { IsString } from 'class-validator';

export class OutletRegisterDTO {
  @IsString()
  name: string;

  @IsString()
  brand: string;
}
