import { Expose } from 'class-transformer';

export class ShowCustomerDTO {
  @Expose()
  id: string;
  @Expose()
  name: string;
  @Expose()
  numberOfOutlets: number;
  @Expose()
  numberOfEmployeesOfCurrentCustomer: number;
}
