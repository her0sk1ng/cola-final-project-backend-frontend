import { IsString } from 'class-validator';

export class CustomerRegisterDTO {
  @IsString()
  name: string;
}
