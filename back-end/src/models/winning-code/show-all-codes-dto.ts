import { Expose } from 'class-transformer';

export class ShowAllCodesDTO {
  @Expose()
  barcode: string;
  @Expose()
  prize: string;
  @Expose()
  redeemed: boolean;
}
