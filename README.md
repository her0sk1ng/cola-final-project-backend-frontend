## Description

This is the Coca Cola Project backend prepared by Krasimir Petkov & Dimitar Markov
For frontend description, please check corresponding folder

## Front-End Installation

```bash
$ npm install in "front-end"
```

## Running the Front-End

```bash
$ ng-s
```

## Installation of Back-End

```bash
$ npm install in "back-end"
```

## Running the Back-End

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

We DO NOT provide support for this project :D

## API Endpoints

1. login User

```bash
POST /api/auth/
```

Accepts request body:

    email: string;
    password: string;

2. login Admin

```bash
POST /api/auth/admin
```

Accepts request body:

    email: string;
    password: string;

3. Logout

```bash
DELETE /api/auth/
```

User must be authorized

4. Create user (guarded for admins only)

```bash
POST /api/users
```

Accepts request body:

    name: string;
    email: string;
    password: string;
    outlet: string;

5. Create customer (guarded for admins only)

```bash
POST /api/customers
```

Accepts request body:

    name: string;

6. Create outlet (guarded for admins only)

```bash
POST /api/outlets
```

Accepts request body:

    name: string;
    brand: string;

7. update user (guarded for admins only)

```bash
POST /api/users/:id
```

Accepts partial body

    name?: string;
    email?: string;
    password?: string;
    outlet?: string;

8. update outlet (guarded for admins only)

```bash
POST /api/outlets/:id
```

Accepts partial body:

    name?: string;
    brand?: string;

9. Delete user (guarded for admins only)

```bash
DELETE /api/users/:id
```

10. Delete outlet (guarded for admins only)

```bash
DELETE /api/users/:id
```

11. Delete customer (guarded for admins only)

```bash
DELETE /api/customers/:id
```

12. Check winning code

```bash
GET /api/codes/:id
```

Returns DTO

    barcode: string;
    prize: string;

13. Add initial code scan operation in activities

```bash
GET /api/codes/scanned/:code
```

Accepts DTO

    code: string;
    winning: boolean;

Returns object with activity Id

13. Update redemption activity record

```bash
PUT /api/codes/scanned/:code
```

Accepts DTO for the updated activity

    id: string;

Returns object with activity Id

14. Get redeemed code activity details:

```bash
GET /api/codes/scanned/:code
```

Returns object with properties:

    id: string;
    outlet: string;
    customer: string
    timestamp: Date;
    username: string;

15. Update activity records with prize(on redeem operation)

```bash
PUT /api/codes/scanned/:code
```

Accepts DTO for the updated activity

    prizeBarcode: string;

Returns object with activity Id

16. Send email to hardcoded admin email address

```bash
POST /api/email
```

Accepts DTO:

    sender: string;
    message: string;
    subject: string;

17. Get all redemption-code activities

```bash
GET api/codes/redemptionactivities
```

Accepts query parameters:

    userId: string;
    outelet: string;

eg:
a) GET /api/codes/redemptionactivities/?userId=025d07dc-725b-4307-8cec-bc4e356edea2
b) GET /api/codes/redemptionactivities/?outlet=Billa

ShowRedemptionCodeActivityDTO[]
Returns redemptioncodes and username;

18. Add login activity(empty body, it uses authenticated user details)

```bash
POST /api/users/loginActivity
```

19. Add logout activity(empty body, it uses authenticated user details)

```bash
POST /api/users/logoutActivity
```

20. Get All User activities

```
GET /api/allUserActivities
```

UserActivityDTO
Returns User Activity - Logout and Login, with names, brands, outlets , timestamps and etc.
